## 项目 copy 步骤
1. 创建 GitHub 上的 demo，并且该项目具有两个远程分支 master + develop
2. 解除远程仓库
    a. 查看本地仓库与远程仓库的关联详情
        `git remote -v`
    b. 解除远程仓库
        `git remote rm origin`
3. 关联另一个仓库
    a. `git remote add origin git@gitlab.com:bowen-wu/migrationprojectdemo.git`
    b. `git push -u origin master`
